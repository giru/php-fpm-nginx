FROM nesix/php-fpm-nginx:7.3-1.17

RUN apk --update --no-cache add \
    build-base \
    autoconf \
    imagemagick \
    imagemagick-dev \
    pcre-dev \
    zlib-dev \
    icu-dev

#RUN apk --update --no-cache add pcre-dev zlib-dev icu-dev && \
#    docker-php-ext-configure intl && \
#    docker-php-ext-install intl opcache

RUN pecl channel-update pecl.php.net && \
    pecl install imagick mongodb && \
    docker-php-ext-configure intl && \
    docker-php-ext-enable imagick mongodb && \
    docker-php-ext-install pdo_mysql bcmath sockets intl opcache
