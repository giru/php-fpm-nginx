FROM php:7.3-fpm-alpine

LABEL maintainer="ruslan@giru.ru"

ENV OPENRESTY_VERSION openresty-1.15.8.2
ENV TZ Europe/Moscow
ENV JWT_KEY KEY

RUN apk --update --no-cache add openssl-dev pcre-dev zlib-dev wget build-base autoconf supervisor tzdata icu-dev imagemagick imagemagick-dev && \
    mkdir /www && chown www-data:www-data /www

RUN pecl channel-update pecl.php.net && \
    pecl install imagick mongodb && \
    docker-php-ext-configure intl && \
    docker-php-ext-install pdo_mysql bcmath sockets intl && \
    docker-php-ext-enable imagick mongodb

RUN mkdir -p /tmp/src && cd /tmp/src && wget https://openresty.org/download/${OPENRESTY_VERSION}.tar.gz && \
    tar -zxvf ${OPENRESTY_VERSION}.tar.gz && cd /tmp/src/${OPENRESTY_VERSION} && \
    ./configure \
        --with-pcre-jit \
        --user=www-data \
        --group=www-data \
        --conf-path=/etc/nginx/nginx.conf \
        --with-http_iconv_module && \
    make && \
    make install && \
    apk del build-base && \
    rm -rf /var/cache/apk/* && \
    rm -rf /tmp/src && \
    apk add luajit && \
    ln -s /usr/local/openresty/bin/opm /usr/local/bin/opm && \
    ln -s /usr/local/openresty/bin/resty /usr/local/bin/resty
#    ln -sf /dev/stdout /var/log/nginx/access.log && \
#    ln -sf /dev/stderr /var/log/nginx/error.log
#        --with-http_redis2_module \
#        --with-http_ssl_module \
#        --with-http_gzip_static_module \
#--http-log-path=/var/log/nginx/access.log \
#--pid-path=/var/run/nginx.pid \
#--error-log-path=/var/log/nginx/error.log \
#--sbin-path=/usr/local/sbin/nginx && \

RUN opm get SkyLothar/lua-resty-jwt

RUN curl -sS https://getcomposer.org/installer | php -- \
    --filename=composer \
    --install-dir=/usr/local/bin

COPY nginx-openresty.conf /etc/nginx/nginx.conf
COPY server-openresty.conf /etc/nginx/server.conf
COPY fastcgi_params /etc/nginx/fastcgi_params
COPY supervisord-openresty.conf /etc/supervisord.conf
COPY php.ini /usr/local/etc/php/php.ini
COPY fpm.conf /usr/local/etc/php-fpm.d/zz-docker.conf
COPY --chown=www-data:www-data *.php /www/public/

WORKDIR /www

EXPOSE 80 8080

STOPSIGNAL SIGTERM

CMD ["supervisord", "-c", "/etc/supervisord.conf"]
