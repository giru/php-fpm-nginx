FROM php:8.1-fpm-alpine

LABEL maintainer="ruslan@giru.ru"

ENV NGINX_VERSION nginx-1.20.2
ENV TZ Europe/Moscow

RUN apk --update --no-cache add \
        openssl-dev pcre-dev zlib-dev icu-dev \
        curl git \
        build-base autoconf \
        supervisor \
        tzdata \
    && mkdir /www \
    && chown www-data:www-data /www \
    && mkdir -p /tmp/src \
    && cd /tmp/src \
    && curl http://nginx.org/download/${NGINX_VERSION}.tar.gz | tar -xz \
    && cd /tmp/src/${NGINX_VERSION} \
    && ./configure \
        --with-pcre \
        --with-http_ssl_module \
        --with-http_gzip_static_module \
        --prefix=/etc/nginx \
        --user=www-data \
        --group=www-data \
        --conf-path=/etc/nginx/nginx.conf \
        --http-log-path=/var/log/nginx/access.log \
        --pid-path=/var/run/nginx.pid \
        --error-log-path=/var/log/nginx/error.log \
        --sbin-path=/usr/local/sbin/nginx \
    && make \
    && make install \
    && docker-php-ext-configure intl \
    && docker-php-ext-install intl \
    && apk del build-base autoconf \
    && rm -rf /var/cache/apk/* \
    && rm -rf /tmp/src \
    && ln -sf /dev/stdout /var/log/nginx/access.log \
    && ln -sf /dev/stderr /var/log/nginx/error.log \
    && curl -sS https://getcomposer.org/installer | php -- \
        --filename=composer \
        --install-dir=/usr/local/bin

COPY nginx.conf /etc/nginx/nginx.conf
COPY supervisord.conf /etc/supervisord.conf
COPY php.ini /usr/local/etc/php/php.ini
COPY fpm.conf /usr/local/etc/php-fpm.d/zz-docker.conf
COPY --chown=www-data:www-data *.php /www/public/

WORKDIR /www

EXPOSE 80

STOPSIGNAL SIGTERM

CMD ["supervisord", "-c", "/etc/supervisord.conf"]
